package buu.example.parkingii
import android.content.Context
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import buu.example.parkingii.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val mySlot1: MySlot = MySlot("","","","ON")
    private val mySlot2: MySlot = MySlot("","","","ON")
    private val mySlot3: MySlot = MySlot("","","","ON")
    private var value: Int = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        hidetextbtn()
        binding.apply {
            slotOneBtn.setOnClickListener {
                value = 1
                datashow(it)
            }
            slotTwoBtn.setOnClickListener {
                value = 2
                datashow(it)
            }
            slotThreeBtn.setOnClickListener {
                value = 3
                datashow(it)
            }
            updateButton.setOnClickListener {
                updateslot(it)
            }
            deleteButtons.setOnClickListener {
                deleteslot(it)
            }
        }
        binding.mySlot1 = mySlot1
        binding.mySlot2 = mySlot2
        binding.mySlot3 = mySlot3
    }

    private fun updateslot(view: View) {
        binding.apply {

            licenseplateOneText.visibility = View.GONE
            brandTwoText.visibility = View.GONE
            cusnameThreeText.visibility = View.GONE
            deleteButtons.visibility = View.GONE
            updateButton.visibility = View.GONE

            if (value == 1) {
                mySlot1?.licenseplate = licenseplateOneText.text.toString()
                mySlot1?.brand = brandTwoText.text.toString()
                mySlot1?.cusname = cusnameThreeText.text.toString()
                mySlot1?.status = licenseplateOneText.text.toString()
                slotOneBtn.setBackgroundColor(Color.RED)
            }
            else if (value == 2){
                mySlot2?.licenseplate = licenseplateOneText.text.toString()
                mySlot2?.brand = brandTwoText.text.toString()
                mySlot2?.cusname = cusnameThreeText.text.toString()
                mySlot2?.status = licenseplateOneText.text.toString()
                slotTwoBtn.setBackgroundColor(Color.RED)
            }
            else {
                mySlot3?.licenseplate = licenseplateOneText.text.toString()
                mySlot3?.brand = brandTwoText.text.toString()
                mySlot3?.cusname = cusnameThreeText.text.toString()
                mySlot3?.status = licenseplateOneText.text.toString()
                slotThreeBtn.setBackgroundColor(Color.RED)
            }
            invalidateAll()
            val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)

        }
    }

    private fun deleteslot(view: View) {
        binding.apply {

            licenseplateOneText.visibility = View.GONE
            brandTwoText.visibility = View.GONE
            cusnameThreeText.visibility = View.GONE
            deleteButtons.visibility = View.GONE
            updateButton.visibility = View.GONE

            if (value == 1) {
                mySlot1?.licenseplate = ""
                mySlot1?.brand = ""
                mySlot1?.cusname = ""
                mySlot1?.status = "ON"
                slotOneBtn.setBackgroundColor(Color.GREEN)
            }
            else if (value == 2){
                mySlot2?.licenseplate = ""
                mySlot2?.brand = ""
                mySlot2?.cusname = ""
                mySlot2?.status = "ON"
                slotTwoBtn.setBackgroundColor(Color.GREEN)
            }
            else {
                mySlot3?.licenseplate = ""
                mySlot3?.brand = ""
                mySlot3?.cusname = ""
                mySlot3?.status = "ON"
                slotThreeBtn.setBackgroundColor(Color.GREEN)
            }
            invalidateAll()
        }
    }
    private fun datashow(view: View){
        binding.apply {
            licenseplateOneText.visibility = View.VISIBLE
            brandTwoText.visibility = View.VISIBLE
            cusnameThreeText.visibility = View.VISIBLE
            deleteButtons.visibility = View.VISIBLE
            updateButton.visibility = View.VISIBLE

            if(value == 1){
                licenseplateOneText.setText(mySlot1?.licenseplate)
                brandTwoText.setText(mySlot1?.brand)
                cusnameThreeText.setText(mySlot1?.cusname)
            }
            else if (value == 2){
                licenseplateOneText.setText(mySlot2?.licenseplate)
                brandTwoText.setText(mySlot2?.brand)
                cusnameThreeText.setText(mySlot2?.cusname)
            }
            else{
                licenseplateOneText.setText(mySlot3?.licenseplate)
                brandTwoText.setText(mySlot3?.brand)
                cusnameThreeText.setText(mySlot3?.cusname)
            }
            invalidateAll()
        }
    }
    private fun settextdata(){
        val slotOne = binding.slotOneBtn
        val slotTwo = binding.slotTwoBtn
        val slotThree = binding.slotThreeBtn
        val update = binding.updateButton
        val delete = binding.deleteButtons

    }
    private fun hidetextbtn(){
        binding.apply {
            licenseplateOneText.visibility = View.GONE
            brandTwoText.visibility = View.GONE
            cusnameThreeText.visibility = View.GONE
            updateButton.visibility = View.GONE
            deleteButtons.visibility = View.GONE
        }
    }
}